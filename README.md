# clone-all-private-repos
This is a CLI written in nodejs that can clone all your Gitlab private repo to current folder with group & subgroup folder structures

## Usage
1. Have nodejs on your machine
2. npm config set @gordonsun:registry=https://gitlab.com/api/v4/packages/npm/
3. npm i @gordonsun/clone-all-private-repos -g
4. export GITLAB_TOKEN=XXXXXXXXXXXXXX && clone-all-private-repos
