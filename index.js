#!/usr/bin/env node

const axios = require('axios')
const dotenv = require('dotenv')
const shell = require('shelljs')

dotenv.config()

const gitlabToken = process.env.GITLAB_TOKEN

const currentDir = process.cwd()

const main = async () => {
    let lastProjectId
    while (true) {
        let url = `https://gitlab.com/api/v4/projects?private_token=${gitlabToken}&visibility=private&archived=false&simple=true&pagination=keyset&per_page=100&order_by=id`
        if (lastProjectId) {
            url += `&id_before=${lastProjectId}`
        }
        const res = await axios.get(url)
        const projects = res.data
        const count = projects.length
        console.log(`Found ${count} projects`)
        for (let i = 0; i < count; i++) {
            const project = projects[i]
            lastProjectId = project.id
            const path = project.path_with_namespace
            console.log(`Processing ${i + 1} of ${count} projects: ${lastProjectId} ${path}`)
            let parentPath
            const lastSlash = path.lastIndexOf('/')
            if (lastSlash) {
                parentPath = path.substring(0, lastSlash)
            }
            if (parentPath) {
                shell.exec(`mkdir -p ${parentPath}`)
                shell.cd(parentPath)
            }
            const cloneUrl = project.web_url + '.git'
            shell.exec(`git clone ${cloneUrl}`)
            if (parentPath) {
                shell.cd(currentDir)
            }
        }
        if (count < 100) {
            console.log(`Processed all projects`)
            break
        }
    }
}
main()
